# ![](Images/Logo.png)

## Overview

AutoMATiCA stands for **Auto**mated **M**uscle and **A**dipose **Ti**ssue **C**omposition **A**nalysis. This repository contains software for the automated segmentation of skeletal muscle, intermusclar adipose tissue, visceral adipose tissue, and subcutaneous adipose tissue of computed tomography scans of the 3rd lumbar vertebrae. Our approach implements the [U-NET](https://arxiv.org/abs/1505.04597) architecture for convolutional neural networks.

### Example analysis
![](Images/Analyzed%20scan.png)

This approach involves 4 seperate neural networks. Each network segments one of the following tissues:
- Skeletal Muscle
- Intermuscular adipose tissue
- Visceral adipose tissue
- Subcutaneous adipose tissue

Output of each network is combined into a single segmentation map.

### Schematic overview of network
![](Images/Network%20schematic.png)

Included within this repository:
1) Source code for AutoMATiCA (includes an annotated example of how to use)
2) A user interface for AutoMATiCA (CPU only, not GPU optimized), which doesn't require installation or any programming knowledge (i.e. 'works out of the box')
3) Source code for the user interface
4) Trained [Tensorflow](https://www.tensorflow.org/)(open source machine learning platform) networks for segmentation

## Downloading AutoMATiCA
There are two options for downloading AutoMATiCA
  1) Using Git to clone the repository (requires installation of Git)
  2) Downloading files from Google Drive (user interface only, see option 1 under "Getting started" below)
  
**Note:** Downloading the **repository from gitlab as a zip file will not work** (Tensorflow models can't be downloaded using this format). If the .h5 files in the Tensorflow models folder are 1KB, these files were incorrectly downloaded.

**Note:** Google Drive repository only works on 64 bit systems 

### Using Git
  1) Git is a file mangement system that can be used to download source code from GitLab. Visit the [Git website](https://git-scm.com/downloads) to download and install Git.
  
  2) Once installed, navigate to the directory/folder you wish to download AutoMATiCA in, right click and select "Git Bash Here". Once the Git Bash terminal loads, type:
  ```
  git clone https://gitlab.com/Michael_Paris/AutoMATiCA.git
  ```
  3) This will download and setup all the necessary files for AutoMATiCA (this will take several minutes to download and setup). Follow the "Getting started" section below for more details on usage.

### Using Google Drive
  1) Visit our [Google Drive](https://drive.google.com/open?id=1mPyJr0K58w-DAjIB4Vh3zoDPo0SHH6DF) repository to download the necessary files. Select "DOWNLOAD ALL" on the upper-right hand side 
  
  2) Once downloaded, upzip and extract AutoMATiCA and follow the "Getting started" section below for more details on usage.

## Getting started
There are several options for implementation, which require varying degrees of Python knowledge. All testing and interface development was done in Windows. The user interface will not run on LINUX or macOS systems.

1) The **easiest approach** is to download the [user interface](AutoMATiCA%20interface%20-%20no%20installation) and follow the included [instructions](AutoMATiCA%20interface%20-%20no%20installation/AutoMATiCA%20user%20interface%20instructions.pdf). This approach does not require any knowledge of Python or installation of software. However, it is CPU based and will be analyze scans at a much slower rate (~10 to 60 seconds/scan depending on CPU core clock) compared to other approaches utilizing a supported GPU (<0.5 second/scan - see below). We suggest a minimum of 8GB of RAM.

2) The **user interface** can also be setup from [source code](AutoMATiCA%20code%20for%20interface). This approach requires setup of a python environment and installation of the required libraries, but it can be optimized for CPU or GPU. 

3) If you would like more control over the analysis, the [source code](AutoMATiCA%20source%20code) is available alongside an example python script. This option can be applied with a CPU or GPU optimizied approach. This approach will provide the most control and quickest analysis time/scan (<0.5 second/scan, GPU dependent). 

**Note:** For users wishing to implement source code approaches, installation and setup of python, tensorflow (optimized for GPU), and some dependencies can be challenging for those unfamiliar with this process. Downloading [Anaconda](https://anaconda.org) (an open source platform for python distriubtion) and using a conda installation packages for [Pydicom](https://pydicom.github.io/pydicom/stable/getting_started.html) (for compressed pixel scans ensure GDCM dependencies are installed: conda install -c conda-forge gdcm) and [GPU optimized tensorflow](https://towardsdatascience.com/tensorflow-gpu-installation-made-easy-use-conda-instead-of-pip-52e5249374bc) can assist with setting up the more difficult python packages.

**Note:** [requirements-cpu.txt](requirements-cpu.txt) and [requirements-gpu.txt](requirements-gpu.txt) can be used for installing the necessary dependcies. However, DICOM files with compressed pixel arrays require additional dependicies (i.e. GDCM - easiest to install with conda-forge) to analyze. Requirements-gpu requires specialized hardware and NVIDIA drivers (not included within this repository).

**Note:** See the [Tensorflow](https://www.tensorflow.org/install/gpu) installation details for information about supported GPUs. 

## Citation

If you use this repository or source code, we would greatly appreciate citing our publication outlining the development of this approach:

[https://www.clinicalnutritionjournal.com/article/S0261-5614(20)30013-3/fulltext](https://www.clinicalnutritionjournal.com/article/S0261-5614(20)30013-3/fulltext)

## Licenses

This software is distributed under the [MIT License](LICENSE), but uses 3rd-party libraries that are distributed under separate licenses (see [LICENSE-3RD-PARTY.txt](LICENSE-3RD-PARTY.txt)). A shorter overview of each license and associated copyright is also available (see [3RD-PARTY-LICENSE-OVERVIEW.txt](3RD-PARTY-LICENSE-OVERVIEW.txt))
